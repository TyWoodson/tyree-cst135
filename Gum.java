package application;


//This is my own code
//Tyree Woodson
public class Gum extends Snack {

	private boolean minty;
	
	public Gum(String n, double p, int q, boolean spicy, boolean containNuts, boolean minty){
		super(n, p, q, spicy, containNuts);
		this.minty = minty;
	}

	public boolean isMinty() {
		return minty;
	}

	public void setMinty(boolean minty) {
		this.minty = minty;
	}
	
}
