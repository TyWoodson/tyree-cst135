package application;
//this is my work
//Tyree Woodson

public abstract class Product {

	private String name;
	private double price;
	private int quantity;
	
	public Product(String n, double p, int q){
		this.price = p;
		this.name = n;
		this.quantity = q;
	}
	
	public Product(){
		this("", 0.0, 0);
	}
	
	public boolean sellOne(){
		if(this.quantity == 0)
			return false; //out of stock
		this.quantity--;
		return true;
	}

public boolean equals (Object o){
	//see if o is a Product
	if(o instanceof Product){
		//o is a Product, so cast o to a Product
		Product p = (Product)o;
		if(p.price == this.price &&
				p.name.equals(this.name))
			return true;
	}
	return false;
}

public double getPrice(){
	return price;
}

public void setPrice(double price){
	this.price = price;
}

public String getName(){
	return name;
}

public void setName(String name){
	this.name = name;
}

public int getQuantity() {
	return quantity;
}

public void setQuantity(int quantity) {
	this.quantity = quantity;
}

@Override
public String toString(){
	return "[Name = " + name + " | Price = " + price + " | Quantity = " + quantity + "] "; 
}

}

