import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class BoundedQ<E> implements Queue<E> {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BoundedQ<String> strQ = new BoundedQ<>(10);
		strQ.add("Test10");
		strQ.add("this");
		strQ.add("is");
		strQ.add("a");
		strQ.add("Test2");
		strQ.add("Test3");
		strQ.add("Test4");
		strQ.add("Test5");
		strQ.add("Test6");
		strQ.add("Test7");
		strQ.add("Test8");

		
		while(!strQ.isEmpty()){
			System.out.println(strQ.size());
			System.out.println(strQ.remove());
			System.out.println(strQ.contains("is"));
			
		}
		System.out.println("Start:" + strQ.peek());
	}

	private int front;
	private int back;
	private int numE;
	private final int CAP;
	

	
	private E[] arr;
	
	public BoundedQ(int capacity){
		CAP = capacity;
		front = back = 0;
		numE = 0;
		arr = (E[]) new Object[CAP];
	}
	
	
	//Required Done
	@Override
	public int size() {
		return this.numE;
	}

	//Required Done
	@Override
	public boolean isEmpty() {
		return numE == 0;
	}

	//Required Done
	@Override
	public boolean contains(Object o) {
		for(int i = front; i < front + numE; i++)
			if(arr[i%CAP].equals(o)) return true;
		return false;
		
	}

	
	//OMIT
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	
	//Required Done
	@Override
	public Object[] toArray() {
		Object [] arr = new Object[numE];
		int indx = 0;
		for(int i = front; i < front + numE; i++){
			arr[indx++] = this.arr[i%CAP];
			
		}
		
		return arr;
	}

	
	//OMIT
	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	//Optional
	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	
	//Required Done
	@Override
	public boolean containsAll(Collection<?> c) {
		for(Object e : c){
			if(!contains(e))
				return false;
		}
		return true;
	}

	//Required Done
	@Override
	public boolean addAll(Collection<? extends E> c) {
		for(E e : c){
			if(!this.add(e))
				return false;
		}
		return true;
	}

	
	//Optional
	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	
	//OMIT
	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	
	//Required Done
	@Override
	public void clear() {
		numE = front = back = 0;
		
	}

	
	//Required Done
	@Override
	public boolean add(E arg0) {
		//see if full
		if(CAP == numE) return false;
		
		//add the element at the index back
		arr[back] = arg0;
		back = (back + 1) % CAP;
		numE++;
		
		return true;
		
	}

	//OMIT
	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	
	//Required Done
	@Override
	public E remove() {
		if (isEmpty()) return null;
		
		E temp = arr[front];
		//move front
		front = (front + 1) % CAP;
		numE--;
		return temp;
	}

	
	//OMIT
	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}

	
	//OMIT
	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}

	
	//Required Done
	@Override
	public E peek() {
		
		return arr[front];
	}

}
