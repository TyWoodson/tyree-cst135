package application;

//This is my own code
//Tyree Woodson
public class Chips extends Snack {

	private boolean cheesy;
	
	public Chips(String n, double p, int q, boolean spicy, boolean containNuts, boolean cheesy){
		super(n, p, q, spicy, containNuts);
		this.cheesy = cheesy;
	}

	public boolean isCheesy() {
		return cheesy;
	}

	public void setCheesy(boolean cheesy) {
		this.cheesy = cheesy;
	}
	
}
