import java.util.Arrays;

public class SortingStuff {
 
	public static void main(String[] args) {
		String [] strArr = {"A", "whatever", "Blank", "Pizza", "Merp", "something", "Temmie", "Bob", "Peridot"};
		
		for(String s : strArr){
			System.out.println("\'" + s + "\'" + " ");
		}
		System.out.println();
		
		Arrays.sort(strArr);
		
		for(String s : strArr){
			System.out.println("\'" + s + "\'" + " ");
		}
		System.out.println();
		
		Product[] prodArr = new Product[5];
		prodArr[0] = new Product ("X", .75, 2);
		prodArr[1] = new Product ("Y", 1.25, 1);
		prodArr[2] = new Product ("X", .35, 8);
		prodArr[3] = new Product ("W", 1.55, 2);
		prodArr[4] = new Product ("Z", .25, 0);
		
		for(Product s : prodArr){
			System.out.println("\'" + s + "\'" + " ");
		}
		System.out.println();
		
		Arrays.sort(prodArr);
		for(Product s : prodArr){
			System.out.println("\'" + s + "\'" + " ");
		}
		System.out.println();
	}
}

class Product implements Comparable<Product>{
	private String name;
	private double price;
	private int quantity;
	
	public Product(String n, double p, int q){
		this.name = n;
		this.price = p;
		this.quantity = q;
	}
		public String toSting(){
			return "[Name: " + name + ", Price: " + price + ", Quantity:" + quantity +  "]";
		}
	
	


	@Override
	public int compareTo(Product arg0) {
		if(this.name.compareToIgnoreCase(arg0.name) < 0)
			return -1;
		else if(this.name.compareToIgnoreCase(arg0.name) > 0)
			return 1;
		else if(this.price > arg0.price)
			return 1;
		else if(this.price < arg0.price)
			return -1;
		return 0;
	}
}
