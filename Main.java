package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class Main extends Application {
	private GridPane gp = new GridPane();
	private Dispencer d = new Dispencer();
	private Button boss;

	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			loadGrid();
			root.setCenter(gp);
			boss = new Button("Boss");
			boss.setFont(Font.font("Papyrus", 20));
			boss.setOnAction(vendButtonHandler);
			root.setRight(boss);
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);

			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	public static void main(String[] args) {
		launch(args);
	}

	private void loadGrid() { // set padding around the grid
		// gp.setPadding(new Insets(10));

		int i = 0;
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 2; c++) {
				Product prod = d.getProduct(i++);
				if (prod != null) {
					ProductBox p = new ProductBox(prod);
					gp.add(p, r, c);
				}
			}
		}
		System.out.println(gp);
	}

	/*
	 * public <ItemBox> void start(Stage primaryStage) throws Exception{
	 * 
	 * StackPane sp = new StackPane(); ProductBox ProductBox = new
	 * ProductBox(null); }
	 */

EventHandler<ActionEvent> vendButtonHandler = new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		System.out.println("Test");
	}
};}