package application;

//This is my own code
//Tyree Woodson
public class Candy extends Snack {

	private boolean chocolate;
	
	public Candy(String n, double p, int q, boolean spicy, boolean containNuts, boolean chocolate){
		super(n, p, q, spicy, containNuts);
		this.chocolate = chocolate;
	}

	public boolean isChocolate() {
		return chocolate;
	}

	public void setChocolate(boolean chocolate) {
		this.chocolate = chocolate;
	}
	
}