package application;



/**
 This is my own work 
  @author Tyree Woodson
 Milestone 2
 CST135, Spring 2017
 **/
public class Dispencer {

	
	
	private final int MAX = 10;
	private Product [] items;
	private int numItems;
	
	//Constructor
	public Dispencer(){
		items = new Product[MAX];
		numItems = 0;
		items[numItems++] = new Drink("Cola" , 1.75 , 5 , true , false);
		items[numItems++] = new Chips("Doritos" , 1.25 , 2 , true , false , false);
		items[numItems++] = new Gum("Some Gum", .25, 9, false, false, true);
		items[numItems++] = new Candy("Candy Bar", 1.25, 6, false, true, true);
		items[numItems++] = new Drink("Water" , 0.75, 15, false, true);
		
		
	}
	
	public Product getProduct(String name){
		for(int i = 0; i<numItems; i++){
			if(items[i].getName().equalsIgnoreCase(name))
				return items[i];
		}
		return null;
	}
	public Product getProduct(int index){
		if(index < numItems){
			return items[index];
		}
		else return null;
	}
	
	
	public boolean addProduct(Product p){
		if(numItems == MAX)return false;
		items[numItems] = p;
		numItems++;
		return true;
		
	}
	public boolean removeProduct(Product p){
		//find the product
		int index = findProduct(p);
		if(index == -1) return false;
		//move item at [numItems-1] to index of p
		items[index] = items[numItems-1];
		//reduce numItems
		return true;
	}
	
	public int findProduct(Product p){
		for(int i = 0; i < numItems; i++){
			if(items[i].equals(p))
				return i;
		}
		return -1;
	}
	
	
	public void changePrice(Product p, double price){
		//find p
		int index = findProduct(p);
		if(index == -1) return;
		//update price
		items[index].setPrice(price);
		}
	public void restockProduct(Product p, double price){
		//find p
		int index = findProduct(p);
		if(index == -1) return;
		//update quantity
		items[index].setQuantity(numItems);
		
	}
	
	//display products
	public void displayProducts(){
		for(int i = 0; i<numItems; i++){
			System.out.println(items[i]);
			
			//loadGrid();
			//ProductBox();
		}
	}
	
	
}
