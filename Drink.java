package application;


//This is my own code
//Tyree Woodson
public class Drink extends Product {

	private boolean carbonated, sugarFree;

	public Drink(String n, double p, int q, boolean carbonated, boolean sugarFree) {
		super(n, p, q);
		this.carbonated = carbonated;
		this.sugarFree = sugarFree;
	}
	
	public Drink(){
		super();
		this.carbonated = true;
		this.sugarFree = false;
	}
	
	public Drink(Drink d){
	
		this.carbonated = d.carbonated;
		this.sugarFree = d.sugarFree;
	}

	public boolean isCarbonated() {
		return carbonated;
	}

	public void setCarbonated(boolean carbonated) {
		this.carbonated = carbonated;
	}

	public boolean isSugarFree() {
		return sugarFree;
	}

	public void setSugarFree(boolean sugarFree) {
		this.sugarFree = sugarFree;
	}
	
}
