package application;
//This is my own code
//Tyree Woodson
public abstract class Snack extends Product {

	private boolean spicy, containNuts;
	
	public Snack(String n, double p, int q, boolean spicy, boolean containNuts){
		super(n, p, q);
		this.spicy = spicy;
		this.containNuts = containNuts;
	}
	
	public Snack(){
		super();
		this.spicy = false;
		this.containNuts = false;
	}
	
	public Snack(Snack s){
		
		this.spicy = s.spicy;
		this.containNuts = s.containNuts;
	}

	public boolean isSpicy() {
		return spicy;
	}

	public void setSpicy(boolean spicy) {
		this.spicy = spicy;
	}

	public boolean isContainNuts() {
		return containNuts;
	}

	public void setContainNuts(boolean containNuts) {
		this.containNuts = containNuts;
	}
	
}
