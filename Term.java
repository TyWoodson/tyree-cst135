package application;


public class Term{
	private double coefficient;
	private int exponent;
	
	public Term(double c, int e){
		coefficient = c;
		exponent = e;
	}
	
	//add getters and setters
		public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	public int getExponent() {
		return exponent;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	//complete the .equals method
	@Override
	public boolean equals(Object o){
		return (boolean) o;
	}


}
    