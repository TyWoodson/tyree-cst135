package application;
//This is my own code

//Tyree Woodson

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.layout.HBox;

public class ProductBox extends VBox {

	private ImageView imgView;
	private Image img;
	private Button btn;
	private Text txtName;
	private Product r;

	public ProductBox(Product p) {
		this.r = p;
		// create button
		btn = new Button();
		btn.setPrefSize(150, 150);

		// set pic baised on type
		if (p instanceof Chips) {
			imgView = new ImageView(new Image("Chips.png"));
			imgView.setFitHeight(150);
			imgView.setPreserveRatio(true);
		} else if (p instanceof Drink) {
			imgView = new ImageView(new Image("Drink.png"));
			imgView.setFitHeight(150);
			imgView.setPreserveRatio(true);
		} else if (p instanceof Candy) {
			imgView = new ImageView(new Image("Candy.png"));
			imgView.setFitHeight(150);
			imgView.setPreserveRatio(true);
		} else if (p instanceof Gum) {
			imgView = new ImageView(new Image("Gum.png"));
			imgView.setFitHeight(150);
			imgView.setPreserveRatio(true);
		} else {
			imgView = new ImageView(new Image("Default.png"));
			imgView.setFitHeight(150);
			imgView.setPreserveRatio(true);
		}

		btn.setText("$" + p.getPrice() + " Remaining: " + p.getQuantity());
		btn.setWrapText(true);

		// set label text and font
		txtName = new Text(p.getName());
		txtName.setFont(Font.font("Comic Sans MS", 30));

		btn.setFont(Font.font("Futura", 20));
		this.getChildren().addAll(imgView, txtName, btn);
		this.setPadding(new Insets(10));
		this.setSpacing(10);
		btn.setOnAction(vendButtonHandler);
	}

	EventHandler<ActionEvent> vendButtonHandler = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			// Identify button the t fired the event
			Button b = (Button) event.getSource();
			// get buttons parent
			ProductBox p = (ProductBox) b.getParent();
			// get text property of the parent
			String prodName = p.txtName.getText();
			if (r.getQuantity() > 0) {
				r.sellOne();
				btn.setText("$" + r.getPrice() + " Remaining:" + r.getQuantity());
				// Display alert
				Alert alert = new Alert(AlertType.INFORMATION, "Now dispensing your " + prodName);
				alert.showAndWait();
			} else {
				Alert alert = new Alert(AlertType.WARNING, prodName + " is out of stock.");
				alert.showAndWait();
			}
		}

	};

}
